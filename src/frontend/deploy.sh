echo "...delete frontend deploy..."
kubectl delete -f ../../kubernetes-manifests/frontend.yaml

echo "...docker build..."
sudo docker build . -t frontend

echo "...minikube rm..."
minikube image rm frontend

echo "...minikube load..."
minikube image load frontend

echo "...apply frontend deploy..."
kubectl apply -f ../../kubernetes-manifests/frontend.yaml